﻿using System.Collections.Generic;

namespace LightPWA.Services.Contracts.Models.Dtos
{
    public class UserDto
    {
        public UserDto()
        {
            Genres = new List<GenreDto>();
        }

        public string Id { get; set; }
                
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public IList<GenreDto> Genres { get; set; }
    }
}
