﻿
namespace LightPWA.Services.Contracts.Models.Dtos
{
    public class GenreDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
