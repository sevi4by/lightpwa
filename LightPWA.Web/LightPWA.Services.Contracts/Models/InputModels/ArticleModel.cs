﻿using LightPWA.Services.Contracts.Models.Dtos;
using System.Collections.Generic;

namespace LightPWA.Services.Contracts.Models.InputModels
{
    public class ArticleModel
    {
        public string Text { get; set; }

        public string Name { get; set; }

        public string AuthorId { get; set; }
        
        public ICollection<int> GenresId { get; set; }        
    }
}
