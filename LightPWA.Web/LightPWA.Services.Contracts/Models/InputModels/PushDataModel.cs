﻿using System.Collections.Generic;

namespace LightPWA.Services.Contracts.Models.InputModels
{
    public class PushDataModel
    {
        public PushDataModel()
        {
            GenresId = new List<int>();
        }
        public string UserId { get; set; }

        public string PushEndpoint { get; set; }

        public string PushP256DH { get; set; }

        public string PushAuth { get; set; }

        public ICollection<int> GenresId { get; set; }
    }
}
