﻿using System.Collections.Generic;

namespace LightPWA.Services.Contracts.Models.PushData
{
    public class Notification
    {
        public string Title { get; set; }

        public string Body { get; set; }

        public string Icon { get; set; }

        public UrlData Data { get; set; }

        public IList<Actional> Actions { get; set; }
    }
}
