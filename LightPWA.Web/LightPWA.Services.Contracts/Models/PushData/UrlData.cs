namespace LightPWA.Services.Contracts.Models.PushData
{
    public class UrlData
    {
        public string Url { get; set; }
    }
}
