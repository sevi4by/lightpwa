﻿
namespace LightPWA.Services.Contracts.Models.PushData
{
    public class Payload
    {
        public Notification Notification { get; set; }
    }
}
