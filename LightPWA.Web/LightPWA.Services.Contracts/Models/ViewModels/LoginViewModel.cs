﻿using System.ComponentModel.DataAnnotations;

namespace LightPWA.Services.Contracts.Models.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }        
        
    }
}
