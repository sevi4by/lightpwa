﻿using LightPWA.Services.Contracts.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightPWA.Services.Contracts.Models.ViewModels
{
    public class ArticleViewModel
    {
        public ArticleViewModel()
        {
            GenresId = new List<int>();
        }

        public string Text { get; set; }

        public string Name { get; set; }

        public ICollection<int> GenresId { get; set; }

        public string AuthorFirstName{ get; set; }

        public string AuthorLastName { get; set; }
    }
}
