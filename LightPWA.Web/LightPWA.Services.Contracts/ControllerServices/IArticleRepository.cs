﻿using LightPWA.Data.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightPWA.Services.Contracts.ControllerServices
{
    public interface IArticleRepository
    {
        Task<IQueryable<Article>> GetAllArticles();
    }
}
