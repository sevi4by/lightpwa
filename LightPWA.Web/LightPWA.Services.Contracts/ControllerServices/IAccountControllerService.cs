﻿using LightPWA.Services.Contracts.Models.Dtos;
using LightPWA.Services.Contracts.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace LightPWA.Services.Contracts.ControllerServices
{
    public interface IAccountControllerService
    {
        Task<IdentityResult> Registration(RegisterViewModel model);

        Task<UserDto> Login(LoginViewModel model);

        void LogOffAsync();
    }
}
