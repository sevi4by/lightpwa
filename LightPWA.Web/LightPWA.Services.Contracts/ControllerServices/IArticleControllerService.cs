﻿using LightPWA.Services.Contracts.Models.InputModels;
using LightPWA.Services.Contracts.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LightPWA.Services.Contracts.ControllerServices
{
    public interface IArticleControllerService
    {
        Task<ArticleViewModel> CreateArticle(ArticleModel article);

        Task<IList<ArticleViewModel>> GetAllArticlesAsync(); 
    }
}
