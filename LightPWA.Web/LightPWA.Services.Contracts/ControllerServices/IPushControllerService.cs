﻿using LightPWA.Services.Contracts.Models.InputModels;
using System.Threading.Tasks;

namespace LightPWA.Services.Contracts.ControllerServices
{
    public interface IPushControllerService
    {
        Task<PushDataModel> SavePushDataAsync(PushDataModel pushData);
    }
}
