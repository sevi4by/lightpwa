﻿using System.Threading.Tasks;
using LightPWA.Data.Contracts.Entities;
using LightPWA.Data.Contracts.Repositories;
using LightPWA.Data.Contracts.UnitOfWork;
using LightPWA.Services.Contracts.ControllerServices;
using LightPWA.Services.Contracts.Models.Dtos;
using LightPWA.Services.Contracts.Models.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace LightPWA.Services.ControllerServices
{
    public class AccountControllerService : IAccountControllerService
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IUnitOfWork unitOfWork;

        public AccountControllerService(UserManager<User> userManager, SignInManager<User> signInManager,
            IUnitOfWork unitOfWork)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.unitOfWork = unitOfWork;
        }

        public async Task<UserDto> Login(LoginViewModel model)
        {          
            var user = await unitOfWork.UserRepository.GetUserByLogin(model.Login);
            await signInManager.PasswordSignInAsync(model.Login, model.Password, false, false);

            return EntityToDtoMapper(user);
        }

        public async Task<IdentityResult> Registration(RegisterViewModel model)
        {
            User user = new User { UserName = model.Login, FirstName = model.FirstName, LastName = model.LastName };

            return await userManager.CreateAsync(user, model.Password);
        }

        public async void LogOffAsync()
        {
            await signInManager.SignOutAsync();
        }

        private UserDto EntityToDtoMapper(User user)
        {
            UserDto userResult = new UserDto
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName
            };

            foreach (var genre in user.Genres)
            {
                userResult.Genres.Add(new GenreDto
                {
                    Id = genre.GenreId,
                    Name = genre.Genre.Name
                });
            }

            return userResult;            
        }
    }
}
