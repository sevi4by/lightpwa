﻿using LightPWA.Data.Contracts.Entities;
using LightPWA.Data.Contracts.UnitOfWork;
using LightPWA.Services.Contracts.ControllerServices;
using LightPWA.Services.Contracts.Models.InputModels;
using LightPWA.Services.Contracts.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebPush;
using LightPWA.Services.Contracts.Models.PushData;
using Newtonsoft.Json;

namespace LightPWA.Services.ControllerServices
{
    public class ArticleControllerService : IArticleControllerService
    {
        private IUnitOfWork unitOfWork;

        public ArticleControllerService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<ArticleViewModel> CreateArticle(ArticleModel article)
        {
            var result = await unitOfWork.ArticleRepository.CreateAsync(InputArticleToEntity(article));
            await unitOfWork.SaveAsync();
            var pushDataResult = await unitOfWork.PushDataRepository.GetPushDatesByGenre(article.GenresId).ToListAsync();

            var data = pushDataResult.Where(x => article.GenresId.Intersect(x.Genres.Select(y => y.GenreId)) != null);
            await WebPushServiceCall(article, data);

            return await EntityArticleToView(result);

        }

        public async Task<IList<ArticleViewModel>> GetAllArticlesAsync()
        {
            var result = unitOfWork.ArticleRepository.GetAllArticles();
            return await EntityArticleCollectionToView(result);
        }

        private Article InputArticleToEntity(ArticleModel articleInput)
        {
            Article article = new Article
            {
                AuthorId = articleInput.AuthorId,
                Name = articleInput.Name,
                Text = articleInput.Text
            };

            foreach (var genreId in articleInput.GenresId)
            {
                article.Genres.Add(new ArticleGenre
                {
                    GenreId = genreId
                });
            }

            foreach (var genre in article.Genres)
            {
                genre.Genre = null;
            }

            return article;
        }

        private async Task WebPushServiceCall(ArticleModel article, IEnumerable<PushData> pushDataArray)
        {
            Payload payload = new Payload
            {
                Notification = new Notification
                {
                    Body = article.Name,
                    Title = "New Article",
                    Icon = "assets/icons/pl-logo.png",
                    Actions = new List<Actional>(),
                    Data = new UrlData
                    {
                        Url = "http://127.0.0.1:8080"
                    }
                }

            };

            Actional actional = new Actional
            {
                Action = "explore",
                Title = "Go"
            };

            payload.Notification.Actions.Add(actional);

            var vapidDetails = new VapidDetails("mailto:smalejk@gmail.com", "BPgsjiSOfZuen4FvUi06CoIG8s4QoA3eRBIpw_ILSReG1zRFFI22_b-q41Z_IX5_IdcN_lJtEfCv8DwnFD9phYI",
                    "EYmj-N3t89vl5YpzBMFD5sNlIpuzdXHgoJvZ9FMIlcM");

            foreach (var pushData in pushDataArray)
            {
                if (article.AuthorId != pushData.UserId)
                {
                    var pushSubscription = new PushSubscription(pushData.PushEndpoint, pushData.PushP256DH, pushData.PushAuth);

                    var webPushClient = new WebPushClient();
                    webPushClient.SendNotification(pushSubscription, JsonConvert.SerializeObject(payload).ToLower(), vapidDetails);
                }
            }
        }

        private async Task<ArticleViewModel> EntityArticleToView(Article article)
        {
            if (article.Author == null)
            {
                article.Author = await unitOfWork.UserRepository.GetUserByIdAsync(article.AuthorId);
            }

            ArticleViewModel articleView = new ArticleViewModel
            {
                Name = article.Name,
                Text = article.Text,
                AuthorLastName = article.Author.LastName,
                AuthorFirstName = article.Author.FirstName
            };

            if (article.Genres != null)
            {
                foreach (var genre in article.Genres)
                {
                    articleView.GenresId.Add(genre.GenreId);
                }
            }

            return articleView;
        }

        private async Task<IList<ArticleViewModel>> EntityArticleCollectionToView(IQueryable<Article> articles)
        {
            List<ArticleViewModel> articleViews = new List<ArticleViewModel>();
            foreach (var article in articles)
            {
                ArticleViewModel articleView = new ArticleViewModel
                {
                    Name = article.Name,
                    Text = article.Text,
                    AuthorLastName = article.Author.LastName,
                    AuthorFirstName = article.Author.FirstName
                };

                if (article.Genres != null)
                {
                    foreach (var genre in article.Genres)
                    {
                        articleView.GenresId.Add(genre.GenreId);
                    }
                }
                articleViews.Add(articleView);
            }

            return articleViews;
        }
    }
}
