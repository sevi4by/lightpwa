﻿using LightPWA.Data.Contracts.Entities;
using LightPWA.Data.Contracts.UnitOfWork;
using LightPWA.Services.Contracts.ControllerServices;
using LightPWA.Services.Contracts.Models.InputModels;
using System.Threading.Tasks;

namespace LightPWA.Services.ControllerServices
{
    public class PushControllerService : IPushControllerService
    {
        private IUnitOfWork unitOfWork;

        public PushControllerService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }


        public async Task<PushDataModel> SavePushDataAsync(PushDataModel pushData)
        {
            var pushDataEntity = InputToEntityPushData(pushData);
            var result = await unitOfWork.PushDataRepository.CreateAsync(pushDataEntity);
            await unitOfWork.SaveAsync();


            
            return pushData;
        }

        private PushData InputToEntityPushData(PushDataModel pushDataInput)
        {
            PushData pushData = new PushData
            {
                PushAuth = pushDataInput.PushAuth,
                PushEndpoint = pushDataInput.PushEndpoint,
                PushP256DH = pushDataInput.PushP256DH,
                UserId = pushDataInput.UserId
            };

            foreach (var genreId in pushDataInput.GenresId)
            {
                pushData.Genres.Add(new PushDataGenre
                {
                    GenreId = genreId
                });
            }


            foreach (var genre in pushData.Genres)
            {
                genre.Genre = null;
                genre.PushData = null;
            }

            return pushData;
        }
    }
}
