﻿using LightPWA.Services.Contracts.ControllerServices;
using LightPWA.Services.Contracts.Models.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LightPWA.Web.Controllers
{
    [Route("api/account/")]
    [EnableCors("MyPolicy")]
    public class AccountController : Controller
    {
        private readonly IAccountControllerService accountControllerService;

        public AccountController(IAccountControllerService accountControllerService)
        {
            this.accountControllerService = accountControllerService;
        }

        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await accountControllerService.Registration(model);
            if (result.Succeeded)
            {
                return Ok();
            }

            return BadRequest();
        }

        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await accountControllerService.Login(model);

            if (result != null)
            {
                return Ok(result);
            }

            return BadRequest();
        }

        [Route("logoff")]
        [HttpPost]
        public IActionResult LogOff()
        {
            accountControllerService.LogOffAsync();

            return Ok();
        }
    }
}
