﻿using LightPWA.Services.Contracts.ControllerServices;
using LightPWA.Services.Contracts.Models.InputModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LightPWA.Web.Controllers
{
    [Route("api/push/")]
    [EnableCors("MyPolicy")]
    public class PushController : Controller
    {
        private IPushControllerService pushControllerService;


        public PushController(IPushControllerService pushControllerService)
        {
            this.pushControllerService = pushControllerService;
        }

        [Route("savePushData")]
        [HttpPost]
        public async Task<IActionResult> SavePushData([FromBody]PushDataModel dataModel)
        {
            var result = await pushControllerService.SavePushDataAsync(dataModel);

            if (result != null)
            {
                return Ok(result);
            }

            return BadRequest();
        }
    }
}