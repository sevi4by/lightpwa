﻿using LightPWA.Services.Contracts.ControllerServices;
using LightPWA.Services.Contracts.Models.InputModels;
using LightPWA.Services.Contracts.Models.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LightPWA.Web.Controllers
{
    [Route("api/article/")]
    [EnableCors("MyPolicy")]
    public class ArticleController : Controller
    {
        private IArticleControllerService articleControllerService;

        public ArticleController(IArticleControllerService articleControllerService)
        {
            this.articleControllerService = articleControllerService;
        }

        [Route("createArticle")]
        [HttpPost]
        public async Task<IActionResult> CreateArticle([FromBody]ArticleModel article)
        {
            var result = await articleControllerService.CreateArticle(article);

            if (result != null)
            {
                return Ok(result);
            }

            return BadRequest();     
        }

        [Route("getArticle")]
        [HttpGet]
        public async Task<IActionResult> GetAllArticles()
        {
            var result = await articleControllerService.GetAllArticlesAsync();

            if (result != null)
            {
                return Ok(result);
            }

            return BadRequest();
        }
    }
}