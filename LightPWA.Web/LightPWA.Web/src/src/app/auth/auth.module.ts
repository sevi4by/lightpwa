import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthComponent} from './auth.component';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {AuthRoutingModule} from './auth-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {MatCardModule} from "@angular/material";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material";

import {MatInputModule} from '@angular/material/input';
import {CoreModule} from "../core/core.module";

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    RegistrationComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    CoreModule
  ],


})
export class AuthModule {
}
