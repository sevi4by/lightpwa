import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../core/services/auth.service";
import {MatSnackBar} from '@angular/material';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  private user;

  constructor(private auth: AuthService,
              public snackBar: MatSnackBar,
              private router: Router) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    })
  }

  errorNotification(errorMessage) {
    this.snackBar.open('Error', errorMessage, { duration: 3000})
  }

  login() {
    this.auth.login(this.loginForm.value).subscribe(
      (user) => {
        this.user = user;
        this.auth.changeUserDataSource(this.user);
        localStorage.setItem('userData', JSON.stringify(this.user));
        this.router.navigate([`/feed`])
      },
      (error) =>  this.errorNotification(error.message)
    )
  }

}
