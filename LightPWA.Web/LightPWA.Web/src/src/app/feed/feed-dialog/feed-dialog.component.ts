import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Genres} from "../../core/constants/genres.const";


@Component({
  selector: 'app-feed-dialog',
  templateUrl: './feed-dialog.component.html',
  styleUrls: ['./feed-dialog.component.scss']
})
export class FeedDialogComponent implements OnInit {

  public addArticleForm: FormGroup;
  public GENRES = Genres;

  constructor(private fb: FormBuilder,
              public dialogRef: MatDialogRef<FeedDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data) {

  }

  ngOnInit() {
    this.addArticleForm = this.fb.group({
      name: ['', Validators.required],
      genresId: ['', Validators.required],
      text: ['', Validators.required]
    })
  }

  onCancelClick(): void {
    this.dialogRef.close(null);
  }


}
