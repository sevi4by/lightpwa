import {Component, OnInit} from '@angular/core';
import {ArticleModel} from "../core/models/article.model";
import {FeedService} from "../core/services/feed.service";
import {AuthService} from "../core/services/auth.service";
import {SubscriptionModel} from "../core/models/subscription.model";

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {

  private user;

  public articles: ArticleModel[] = [
    {
      name: null,
      authorFirstName: null,
      authorLastName: null,
      genresId: null,
      date: null,
      text: null
    }
  ];

  constructor(private feedService: FeedService,
              private auth: AuthService) {
  }

  ngOnInit() {
    this.auth.userInfo.subscribe(user => this.user = user);
    this.getAlArticles();
  }

  getAlArticles() {
    this.feedService.getArticles().subscribe(articles => {
      this.articles = articles;
    })
  }

  createArticle(article) {
    if(article) {
      const newArticle = article;
      newArticle.authorId = this.user.id;
      this.feedService.createArticle(newArticle).subscribe(res => {
        this.feedService.getArticles().subscribe(articles => this.articles = articles)
      })
    }

  }


  subscribeUser(subscribe) {
    const sub = subscribe.subStr;
    const sendValue: SubscriptionModel = {
      userId: this.user.id,
      pushEndpoint: sub.endpoint,
      pushP256DH: sub.keys.p256dh,
      pushAuth: sub.keys.auth,
      genresId: subscribe.result.genres,
    };

    this.feedService.subscribeUser(sendValue).subscribe(
      () => console.log('User successfully subscribed', sendValue),
      (error) => console.log(error)
      )

  }


}
