import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {MatDialog} from "@angular/material";
import {FeedDialogComponent} from "../feed-dialog/feed-dialog.component";
import {UserSubscribeComponent} from "../user-subscribe/user-subscribe.component";
import {PushPublicKey} from "../../core/constants/push-key.const";
import {SwPush} from "@angular/service-worker";
import {AuthService} from "../../core/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() onCreateArticle: EventEmitter<any> = new EventEmitter();
  @Output() onSubscribeUser: EventEmitter<any> = new EventEmitter();

  public user;

  constructor(public dialog: MatDialog,
              private swPush: SwPush,
              private auth: AuthService,
              private router: Router) {
  }

  ngOnInit() {

    this.auth.userInfo.subscribe((user) => this.user = user)
  }

  openAddArticleDialog() {
    const dialogRef = this.dialog.open(FeedDialogComponent, {
      width: '500px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.onCreateArticle.emit(result);
      });
  }

  openSubscribeDialog() {

    this.swPush.requestSubscription({
      serverPublicKey: PushPublicKey
    })
      .then(sub => {
        const subStr = sub.toJSON();

        const dialogRef = this.dialog.open(UserSubscribeComponent, {
          width: '500px',
          data: {}
        });

        dialogRef.afterClosed().subscribe(result => {

          this.onSubscribeUser.emit({result, subStr});
        });
      })
      .catch(err => console.error("Could not subscribe to notifications", err));

  }

  logOut() {
    localStorage.removeItem('userData');
    this.auth.changeUserDataSource(null);

    this.router.navigate(['/auth/login'])
  }

}
