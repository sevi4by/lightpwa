import {Component, OnInit, Input} from '@angular/core';
import {ArticleModel} from "../../core/models/article.model";
import {Genres} from "../../core/constants/genres.const";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  private GENRES = Genres;

  @Input() article: ArticleModel;

  constructor() {
  }

  ngOnInit() {
  }

  getGenreName(id): string {
    return this.GENRES.find(x => x.id === id).name;

  }

}
