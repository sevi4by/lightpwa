import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Genres} from "../../core/constants/genres.const";

@Component({
  selector: 'app-user-subscribe',
  templateUrl: './user-subscribe.component.html',
  styleUrls: ['./user-subscribe.component.scss']
})
export class UserSubscribeComponent implements OnInit {

  public subscribeForm: FormGroup;
  public GENRES = Genres;

  constructor(private fb: FormBuilder,
              public dialogRef: MatDialogRef<UserSubscribeComponent>,
              @Inject(MAT_DIALOG_DATA) public data) {

  }

  ngOnInit() {
    this.subscribeForm = this.fb.group({
      genres: ['', Validators.required]
    })
  }

  onCancelClick(): void {
    this.dialogRef.close(null);
  }

}
