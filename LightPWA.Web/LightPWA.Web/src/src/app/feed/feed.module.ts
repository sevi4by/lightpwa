import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedComponent } from './feed.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatCardModule, MatFormFieldModule, MatInputModule, MatMenuModule} from "@angular/material";
import {MatToolbarModule} from "@angular/material";
import {MatButtonModule} from "@angular/material";
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from "@angular/material";
import { HeaderComponent } from './header/header.component';
import { ArticleComponent } from './article/article.component';
import { FeedDialogComponent } from './feed-dialog/feed-dialog.component';
import { UserSubscribeComponent } from './user-subscribe/user-subscribe.component';

@NgModule({
  declarations: [FeedComponent, HeaderComponent, ArticleComponent, FeedDialogComponent, UserSubscribeComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatChipsModule,
    MatIconModule,
    MatMenuModule
  ],
  entryComponents: [FeedDialogComponent, UserSubscribeComponent]
})
export class FeedModule { }
