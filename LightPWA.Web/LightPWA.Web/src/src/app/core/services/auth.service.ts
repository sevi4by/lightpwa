import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private localUser = this.getLocalUser() ? this.getLocalUser() : null;
  private userDataSource = new BehaviorSubject(this.localUser);
  public userInfo = this.userDataSource.asObservable();

  constructor(private http: HttpClient) { }

  login(data) {
    return this.http.post(`${environment.apiPath}/api/account/login`, data)
      .pipe(
        map(res => {
          return res
        })
      )
  }

  registration(data) {
    return this.http.post(`${environment.apiPath}/api/account/register`, data)
      .pipe(
        map(res => {
          return res
        })
      )
  }

  getLocalUser() {
    return JSON.parse(localStorage.getItem('userData'));
  }

  changeUserDataSource(data) {
    this.userDataSource.next(data);
  }

}
