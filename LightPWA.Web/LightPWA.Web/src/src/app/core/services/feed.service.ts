import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {environment} from "../../../environments/environment";
import {ArticleModel} from "../models/article.model";

@Injectable({
  providedIn: 'root'
})
export class FeedService {

  constructor(private http: HttpClient) {

  }

  getArticles () {
    return this.http.get<ArticleModel[]>(`${environment.apiPath}/api/article/getArticle`)
      .pipe(
        map(res => {
          return res
        })
      )
  }

  createArticle(article) {
    return this.http.post(`${environment.apiPath}/api/article/createArticle`, article)
      .pipe(
        map(res => {
          return res
        })
      )
  }

  subscribeUser (data) {
    return this.http.post(`${environment.apiPath}/api/push/savePushData`, data)
      .pipe(
        map(res => {
          return res
        })
      )
  }
}
