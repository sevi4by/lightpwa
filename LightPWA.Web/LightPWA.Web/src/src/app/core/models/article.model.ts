export class ArticleModel {
  name: string;
  authorFirstName: string;
  authorLastName: string;
  genresId: string[];
  text: string;
  date: string;
}
