export class SubscriptionModel {
  userId: string;
  pushEndpoint: string;
  pushP256DH: string;
  pushAuth: string;
  genresId: number[]
}

