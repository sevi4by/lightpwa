﻿using LightPWA.Data.Contracts.Entities;
using LightPWA.Data.Contracts.Repositories;
using System;
using System.Threading.Tasks;

namespace LightPWA.Data.Contracts.UnitOfWork
{
    public interface IUnitOfWork: IDisposable
    {
        IGenericRepository<Genre> GenreRepository { get; }

        IGenericRepository<ArticleGenre> ArticleGenreRepository { get; }
        
        IUserRepository UserRepository { get; }

        IArticleRepository ArticleRepository { get; }

        IPushDataRepository PushDataRepository { get; }

        Task SaveAsync();
    }
}
