﻿using LightPWA.Data.Contracts.Entities;
using System.Collections.Generic;
using System.Linq;

namespace LightPWA.Data.Contracts.Repositories
{
    public interface IPushDataRepository: IGenericRepository<PushData>
    {
        IQueryable<PushData> GetPushDatesByGenre(ICollection<int> genresId);
    }
}
