﻿using LightPWA.Data.Contracts.Entities;
using System.Linq;

namespace LightPWA.Data.Contracts.Repositories
{
    public interface IArticleRepository: IGenericRepository<Article>
    {
        IQueryable<Article> GetAllArticles();
    }
}
