﻿using LightPWA.Data.Contracts.Entities;
using System.Threading.Tasks;

namespace LightPWA.Data.Contracts.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetUserByLogin(string login);

        Task<User> GetUserByIdAsync(string Id);
    }
}
