﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace LightPWA.Data.Contracts.Repositories
{
    public interface IGenericRepository<TEntity>: IDisposable
        where TEntity : class
    {
        Task<IQueryable<TEntity>> GetAll();

        Task<TEntity> GetByIdAsync(int id);

        Task<TEntity> CreateAsync(TEntity item);

        void Update(TEntity item);

        void Delete(int id);
    }
}
