﻿using LightPWA.Data.Contracts.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace LightPWA.Data.Contracts.Context
{
    public interface IApplicationContext : IDisposable
    {
        DbSet<Article> Articles { get; set; }

        DbSet<Genre> Genres { get; set; }

        DbSet<PushData> PushData { get; set; }
    }
}
