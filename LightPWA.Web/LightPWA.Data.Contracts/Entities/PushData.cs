﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LightPWA.Data.Contracts.Entities
{
    public class PushData
    {
        public PushData()
        {
            Genres = new List<PushDataGenre>();
        }

        public int Id { get; set; }

        public string UserId { get; set; }

        public string PushEndpoint { get; set; }

        public string PushP256DH { get; set; }

        public string PushAuth { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
         
        public virtual ICollection<PushDataGenre> Genres { get; set; }
    }
}
