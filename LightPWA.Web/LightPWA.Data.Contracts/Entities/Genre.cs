﻿using System.Collections.Generic;

namespace LightPWA.Data.Contracts.Entities
{
    public class Genre
    {
        public Genre()
        {
            Articles = new List<ArticleGenre>();
            Users = new List<UserGenre>();
            PushData = new List<PushDataGenre>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<ArticleGenre> Articles { get; set; }

        public virtual ICollection<UserGenre> Users { get; set; }

        public virtual ICollection<PushDataGenre> PushData { get; set; }
    }
}
