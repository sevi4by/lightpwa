﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LightPWA.Data.Contracts.Entities
{
    public class UserGenre
    {
        public string UserId { get; set; }

        public int GenreId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [ForeignKey("GenreId")]
        public virtual Genre Genre { get; set; }
    }
}
