﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LightPWA.Data.Contracts.Entities
{
    public class PushDataGenre
    {
        public int PushDataId { get; set; }
        
        public int GenreId { get; set; }

        [ForeignKey("PushDataId")]
        public virtual PushData PushData { get; set; }

        [ForeignKey("GenreId")]
        [NotMapped]
        public virtual Genre Genre { get; set; }
    }
}
