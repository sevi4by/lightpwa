﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LightPWA.Data.Contracts.Entities
{
    public class Article
    {
        public Article()
        {
            Genres = new List<ArticleGenre>();
        }

        public int Id { get; set; }

        public string Text { get; set; }

        public string AuthorId { get; set; }

        public string Name { get; set; }

        [ForeignKey("AuthorId")]
        public virtual User Author { get; set; }

        public virtual ICollection<ArticleGenre> Genres { get; set; }
    }
}
