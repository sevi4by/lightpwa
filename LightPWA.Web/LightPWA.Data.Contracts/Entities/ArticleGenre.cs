﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LightPWA.Data.Contracts.Entities
{
    public class ArticleGenre
    {
        public int ArticleId { get; set; }

        public int GenreId { get; set; }

        [ForeignKey("ArticleId")]
        public virtual Article Article { get; set; }

        [ForeignKey("GenreId")]
        [NotMapped]
        public virtual Genre Genre { get; set; }
    }
}
