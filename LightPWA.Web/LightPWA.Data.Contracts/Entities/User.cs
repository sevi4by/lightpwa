﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace LightPWA.Data.Contracts.Entities
{
    public class User : IdentityUser
    {
        public User()
        {
            Genres = new List<UserGenre>();
            Articles = new List<Article>();
            PushDates = new List<PushData>();
        }        
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual ICollection<UserGenre> Genres { get; set; }

        public virtual ICollection<Article> Articles { get; set; }

        public virtual ICollection<PushData> PushDates { get; set; }
    }
}
