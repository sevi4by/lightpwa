﻿using LightPWA.Data.Contracts.Context;
using LightPWA.Data.Contracts.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace LightPWA.Data.Context
{
    public class ApplicationContext : IdentityDbContext<User>, IApplicationContext
    {
        public DbSet<Article> Articles { get; set; }

        public DbSet<Genre> Genres { get; set; }

        public DbSet<PushData> PushData { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public ApplicationContext() { }    
        

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ArticleGenre>()
             .HasKey(t => new { t.ArticleId, t.GenreId });

            builder.Entity<ArticleGenre>()
                .HasOne(sc => sc.Article)
                .WithMany(s => s.Genres)
                .HasForeignKey(sc => sc.ArticleId);

            builder.Entity<ArticleGenre>()
                .HasOne(sc => sc.Genre)
                .WithMany(c => c.Articles)
                .HasForeignKey(sc => sc.GenreId);

            builder.Entity<UserGenre>()
             .HasKey(t => new { t.UserId, t.GenreId });

            builder.Entity<UserGenre>()
                .HasOne(sc => sc.User)
                .WithMany(s => s.Genres)
                .HasForeignKey(sc => sc.UserId);

            builder.Entity<UserGenre>()
                .HasOne(sc => sc.Genre)
                .WithMany(c => c.Users)
                .HasForeignKey(sc => sc.GenreId);

            builder.Entity<PushDataGenre>()
             .HasKey(t => new { t.PushDataId, t.GenreId });

            builder.Entity<PushDataGenre>()
                .HasOne(sc => sc.PushData)
                .WithMany(s => s.Genres)
                .HasForeignKey(sc => sc.PushDataId);

            builder.Entity<PushDataGenre>()
                .HasOne(sc => sc.Genre)
                .WithMany(c => c.PushData)
                .HasForeignKey(sc => sc.GenreId);
        }
    }
}
