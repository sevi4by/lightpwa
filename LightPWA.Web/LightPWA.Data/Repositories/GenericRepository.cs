﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LightPWA.Data.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LightPWA.Data.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context;
        protected readonly DbSet<TEntity> DbSet;

        public GenericRepository(DbContext dbContext)
        {
            this.Context = dbContext;
            this.DbSet = Context.Set<TEntity>();
        }

        public async Task<TEntity> CreateAsync(TEntity item)
        {
            var result = await DbSet.AddAsync(item).ConfigureAwait(false);

            return result.Entity;
        }        

        public async Task<IQueryable<TEntity>> GetAll()
        {
            var result = await DbSet.ToListAsync().ConfigureAwait(false);

            return result.AsQueryable();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id).ConfigureAwait(false);
        }

        public void Update(TEntity item)
        {
           DbSet.Update(item);
        }

        public void Delete(int id)
        {
            var entityToDelete = DbSet.Find(id);
            if (entityToDelete != null)
            {
                DbSet.Remove(entityToDelete);
            }
        }

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
