﻿using System.Linq;
using LightPWA.Data.Contracts.Entities;
using LightPWA.Data.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LightPWA.Data.Repositories
{
    public class ArticleRepository : GenericRepository<Article>, IArticleRepository
    {
        public ArticleRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Article> GetAllArticles()
        {
            var result = DbSet.AsQueryable()
                .Include(x => x.Author)
                .Include(x => x.Genres).OrderByDescending(x => x.Id);

            return result;
        }
    }        
}
