﻿using System.Threading.Tasks;
using LightPWA.Data.Contracts.Entities;
using LightPWA.Data.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;


namespace LightPWA.Data.Repositories
{
    public class UserRepository: IUserRepository
    {
        protected readonly DbContext Context;
        protected readonly DbSet<User> DbSet;

        public UserRepository(DbContext dbContext)
        {
            this.Context = dbContext;
            this.DbSet = Context.Set<User>();
        }

        public async Task<User> GetUserByIdAsync(string Id)
        {
            return await DbSet.FirstOrDefaultAsync(x => x.Id == Id).ConfigureAwait(false);
        }

        public async Task<User> GetUserByLogin(string login)
        {
           return await DbSet.FirstOrDefaultAsync(x => x.UserName == login).ConfigureAwait(false);
        }
    }
}
