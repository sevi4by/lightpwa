﻿using LightPWA.Data.Contracts.Entities;
using LightPWA.Data.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LightPWA.Data.Repositories
{
    public class PushDataRepository : GenericRepository<PushData>, IPushDataRepository
    {
        public PushDataRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<PushData> GetPushDatesByGenre(ICollection<int> genresId)
        {
            var result = DbSet.AsQueryable().Include(x => x.Genres);

            return result;
        }
    }
}
