﻿using LightPWA.Data.Context;
using LightPWA.Data.Contracts.Entities;
using LightPWA.Data.Contracts.Repositories;
using LightPWA.Data.Contracts.UnitOfWork;
using LightPWA.Data.Repositories;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace LightPWA.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;
        private readonly IConfiguration _configuration;
        private bool _disposed;

        private IGenericRepository<Genre> _genreRepository { get; set; }
        private IGenericRepository<ArticleGenre> _articleGenreRepository { get; set; }
        private IUserRepository _userRepository { get; set; }
        private IArticleRepository _articleRepository { get; set; }
        private IPushDataRepository _pushDataRepository { get; set; }


        public UnitOfWork(ApplicationContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public IGenericRepository<Genre> GenreRepository =>
          _genreRepository ?? (_genreRepository = new GenericRepository<Genre>(_context));

        public IPushDataRepository PushDataRepository =>
          _pushDataRepository ?? (_pushDataRepository = new PushDataRepository(_context));

        public IGenericRepository<ArticleGenre> ArticleGenreRepository =>
          _articleGenreRepository ?? (_articleGenreRepository = new GenericRepository<ArticleGenre>(_context));

        public IUserRepository UserRepository =>
          _userRepository ?? (_userRepository = new UserRepository(_context));

        public IArticleRepository ArticleRepository =>
          _articleRepository ?? (_articleRepository = new ArticleRepository(_context));


        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}

